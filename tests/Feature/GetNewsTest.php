<?php

namespace Tests\Feature;

use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetNewsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected $user;
    protected $selectedNews;
    protected $count = 5;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->has(News::factory()->count($this->count))->create();
        $this->selectedNews = $this->user->news();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_success()
    {
        $response = $this->actingAs($this->user)->get('/news?page=1');

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(200);

        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('success', $jsonDecoded->result);
        $this->assertIsArray($jsonDecoded->content);
        $this->assertCount($this->count, $jsonDecoded->content);

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_fail()
    {
        $response = $this->get('/news');

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(401);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('error', $jsonDecoded->result);
        $this->assertEquals(null, $jsonDecoded->content);
        $this->assertEquals(1, $jsonDecoded->error_code);
        $this->assertEquals("Unauthenticated", $jsonDecoded->error_des);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_pagination()
    {
        $response = $this->actingAs($this->user)->get('/news?page=2&limit=3');

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(200);

        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('success', $jsonDecoded->result);
        $this->assertIsArray($jsonDecoded->content);
        $this->assertCount(2, $jsonDecoded->content);

    }
}
