<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddNewsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_add_news_success()
    {
        $title = $this->faker->name;
        $content = $this->faker->text;

        $response = $this->actingAs($this->user)->post('/news', [
            'title' => $title,
            'content' => $content,
        ]);

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(200);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('success', $jsonDecoded->result);
        $this->assertEquals($title, $jsonDecoded->content->title);
        $this->assertEquals($content, $jsonDecoded->content->content);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_add_news_fail()
    {
        $title = $this->faker->realTextBetween(300, 400);
        $content = $this->faker->text;

        $response = $this->actingAs($this->user)->post('/news', [
            'title' => $title,
            'content' => $content,
        ]);

        $jsonDecoded = json_decode($response->baseResponse->content());


        $response->assertStatus(400);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('error', $jsonDecoded->result);
        $this->assertEquals(null, $jsonDecoded->content);
        $this->assertEquals(1, $jsonDecoded->error_code);
        $this->assertEquals("The title must not be greater than 255 characters.", $jsonDecoded->error_des);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_add_news_out()
    {


        $title = $this->faker->title;
        $content = $this->faker->text;

        $response = $this->post('/news', [
            'title' => $title,
            'content' => $content,
        ]);

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(401);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('error', $jsonDecoded->result);
        $this->assertEquals(null, $jsonDecoded->content);
        $this->assertEquals(1, $jsonDecoded->error_code);
        $this->assertEquals("Unauthenticated", $jsonDecoded->error_des);
    }
}
