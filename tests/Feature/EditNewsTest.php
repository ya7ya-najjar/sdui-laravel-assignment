<?php

namespace Tests\Feature;

use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditNewsTest extends TestCase
{

    use WithFaker, RefreshDatabase;

    protected $user;
    protected $differentUser;
    protected $selectedNews;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->has(News::factory())->create();
        $this->differentUser = User::factory()->create();
        $this->selectedNews = $this->user->news()->first();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_edit_news_success()
    {
        $title = $this->faker->name;
        $content = $this->faker->text;

        $newsId = $this->selectedNews->id;

        $response = $this->actingAs($this->user)->put("/news/$newsId", [
            'title' => $title,
            'content' => $content,
        ]);

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(200);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('success', $jsonDecoded->result);
        $this->assertEquals($title, $jsonDecoded->content->title);
        $this->assertEquals($content, $jsonDecoded->content->content);

        $this->assertNotEquals($this->selectedNews->title, $jsonDecoded->content->title);
        $this->assertNotEquals($this->selectedNews->content, $jsonDecoded->content->content);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_edit_news_fail()
    {
        $content = $this->faker->text;

        $newsId = $this->selectedNews->id;

        $response = $this->actingAs($this->differentUser)->put("/news/$newsId", [
            'title' => null,
            'content' => $content,
        ]);

        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(403);
        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('error', $jsonDecoded->result);
        $this->assertEquals(null, $jsonDecoded->content);
        $this->assertEquals(1, $jsonDecoded->error_code);
        $this->assertEquals("You don't have the permission to perform this action", $jsonDecoded->error_des);
    }

}
