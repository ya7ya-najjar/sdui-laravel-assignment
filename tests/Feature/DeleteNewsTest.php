<?php

namespace Tests\Feature;

use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteNewsTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $selectedNews;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user =  User::factory()->has(News::factory())->create();
        $this->selectedNews = $this->user->news()->first();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_delete_news_success()
    {
        $newsId = $this->selectedNews->id;
        $response = $this->actingAs($this->user)->delete("/news/$newsId");
        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(200);

        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('success', $jsonDecoded->result);
        $this->assertEquals('Deleted successfully', $jsonDecoded->content);

    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_delete_news_fail()
    {
        newId:
        $newsId = rand(1000, 2000);

        if ($newsId == $this->selectedNews->id)
            goto newId;

        $response = $this->actingAs($this->user)->delete("/news/$newsId");
        $jsonDecoded = json_decode($response->baseResponse->content());

        $response->assertStatus(404);

        $this->assertObjectHasAttribute('result', $jsonDecoded);
        $this->assertObjectHasAttribute('content', $jsonDecoded);
        $this->assertEquals('error', $jsonDecoded->result);
        $this->assertEquals('Item not found', $jsonDecoded->error_des);

    }
}
