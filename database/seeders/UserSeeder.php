<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->firstOrCreate([
            'email' => 'test@sdui.com'
        ],[
            'name' => 'Test User',
            'email' => 'test@sdui.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password')
        ]);
    }
}
