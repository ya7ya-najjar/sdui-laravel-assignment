<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
                return [
                    'direction' => 'in:asc,desc',
                    'field' => 'in:id,title,content,user_id'
                ];
            case 'POST':
                return [
                    'title' => 'required|max:255',
                    'content' => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'title' => 'sometimes|max:255',
                    'content' => 'sometimes'
                ];
            case 'DELETE':
                return [
                ];
            default:break;
        }
        return [];
    }
}
