<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use App\Services\NewsService;
use App\Traits\Responsable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class NewsController extends Controller
{
    private $newsService;
    use Responsable;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $news = $this->newsService->all($request);
        return $this->respondSuccess($news->all(), $this->createTestingPaginator($news));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return JsonResponse
     */
    public function store(NewsRequest $request): JsonResponse
    {
        $news = $this->newsService->add($request);
        return $this->respondSuccess($news);
    }

    /**
     * Display the specified resource.
     *
     * @param News $news
     * @return JsonResponse
     */
    public function show(News $news): JsonResponse
    {
        return $this->respondSuccess($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param News $news
     * @param Request $request
     * @return JsonResponse
     * @throws UnauthorizedException
     */
    public function update(News $news, Request $request): JsonResponse
    {
        $this->newsService->update($news, $request);
        return $this->respondSuccess($news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return JsonResponse
     * @throws UnauthorizedException
     */
    public function destroy(News $news): JsonResponse
    {
        $this->newsService->delete($news);
        return $this->respondSuccess('Deleted successfully');
    }
}
