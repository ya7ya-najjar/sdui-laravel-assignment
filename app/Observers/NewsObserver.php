<?php

namespace App\Observers;

use App\Models\News;
use Illuminate\Support\Facades\Log;
use Psr\Log\LogLevel;

class NewsObserver
{
    public function created(News $news)
    {
        Log::log(LogLevel::INFO, "New news item created with ID $news->id, and Title $news->title");
    }
}
