<?php

namespace App\Services;


use App\Models\News;
use App\Traits\Searchable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class NewsService{

    use Searchable;

    public function add(Request $request): News
    {
        $news = new News($request->all(['title', 'content']));
        $news->user()->associate($request->user());
        $news->save();
        return $news;
    }

    /**
     * @throws UnauthorizedException
     */
    public function update(News $new, Request $request): News
    {
        if ($new->user_id != auth()->id())
            throw new UnauthorizedException("You don't have the permission to perform this action");

        $new->update(array_filter($request->all(['title', 'content'])));
        return $new;
    }

    public function all(Request $request): LengthAwarePaginator
    {
        $news = News::query()->where('user_id', auth()->id());

        if ($search = $request->get('search'))
            $news->where('title', 'LIKE', "%$search%");

        if ($fullSearch = $request->get('full_search')){
            $tokens = $this->convertToSeparatedTokens($fullSearch);
            $news->whereRaw("MATCH(title, content) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }

        if ($request->has(['field', 'direction'])){
            $field = $request->get('field', 'id');
            $direction = $request->get('direction');

            $orderArray = app(News::class)->getFillable();
            $orderArray[] = 'id';

            if(in_array($field, $orderArray))
                $news->orderBy($field, $direction);

        }


        $limit = $request->get('limit') ? : 10 ;
        if($limit > 100 ) $limit = 100;

        return $news->paginate($limit);
    }

    /**
     * @throws UnauthorizedException
     */
    public function delete(News $new)
    {
        if ($new->user_id != auth()->id())
            throw new UnauthorizedException("You don't have the permission to perform this action");

    }
}
