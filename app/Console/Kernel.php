<?php

namespace App\Console;

use App\Jobs\RemoveExpiredNewsJob;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (){
            News::query()
                ->whereDate(
                    'created_at',
                    '<=',
                    Carbon::now()->subDays(14)->toDateString()
                )
                ->delete();
        })->everyMinute();

        // OR

        // $schedule->job(new RemoveExpiredNewsJob)->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
