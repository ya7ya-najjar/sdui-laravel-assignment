<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

trait Responsable
{
    public function respondSuccess($content = [], $paginator = []): JsonResponse
    {
        return response()->json([
            'result' => 'success',
            'content' => $content,
            'paginator' => $paginator,
            'error_des' => '',
            'error_code' => 0,
            'date' =>date('Y-m-d')
        ]);
    }

    public function respondError($message, $validationError = null, $errorCode = 400): JsonResponse
    {
        return response()->json([
            'result' => 'error',
            'content' => null,
            'error_des' => $message,
            'error_validation' => $validationError,
            'error_code' => 1,
            'date' =>date('Y-m-d')
        ], $errorCode);
    }

    public function respondOut($message): JsonResponse
    {
        return response()->json([
            'result' => 'error',
            'content' => null,
            'error_des' => $message,
            'error_code' => -1,
            'date' => date('Y-m-d')
        ]);
    }

    public function createTestingPaginator($data): array
    {
        return [
            'total_count' => $data->total(),
            'limit' => $data->perPage(),
            'total_page' => ceil($data->total() / $data->perPage()),
            'current_page' => $data->currentPage(),
        ];
    }

}
