<?php

namespace App\Traits;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;

trait Searchable{
    function stripPunctuation($string): string
    {
        $string = strtolower($string);
        $string = preg_replace(   '/[[:punct:]]/'   ," ", $string);
        return trim($string);
    }

    function convertToSeparatedTokens($string): string
    {
        $string = $this->stripPunctuation($string);
        $tokenizer = new WhitespaceAndPunctuationTokenizer();
        $tokens = $tokenizer->tokenize($string);
        array_walk($tokens, function(&$value) {
            if($value) {
                $value = $value . '*';
            }
        });
        return implode(' ', $tokens);
    }
}
