<?php

namespace App\Exceptions;

use App\Traits\Responsable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use Responsable;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            return $this->respondError($e->validator->errors()->first(), $e->validator->errors());
        }else if ($e instanceof ModelNotFoundException){
            return $this->respondError('Item not found', null, 404);
        }else if ($e instanceof AuthenticationException){
            return $this->respondError('Unauthenticated', null, 401);
        }else if ($e instanceof NotFoundHttpException){
            return $this->respondError('Wrong url', null, 404);
        }else if ($e instanceof UnauthorizedException){
            return $this->respondError($e->getMessage(), null, 403);
        }

        return parent::render($request, $e);
    }
}
