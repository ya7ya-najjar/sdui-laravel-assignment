# Sdui Test using Laravel 8

### Introduction

...

### Installation

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
touch database/database.sqlite
```

```bash
php artisan key:generate
```

```bash
php artisan migrate --seed
```


```bash
php artisan test
```

```bash
php artisan schedule:work
```

If you would like to monitor/display the database, you can switch the testing type by removing ``` RefreshDatabase ``` in the feature test classes.

### Yahya Alnajjar
Full Stack Developer

...
